package at.spenger.junit.domain;

import java.util.Comparator;

public class Sort implements Comparator<Person>{

	  @Override
	  public int compare(Person p1, Person p2) {
	    
	    String x = p1.getBirthdayString();
	    String y = p2.getBirthdayString() ;
	    return x.compareTo(y);
	  }

}
