package at.spenger.junit.domain;

import java.time.Clock;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.util.Comparator;


public class Person {
	public enum Sex {
		MALE,
		FEMALE
	}
	private String firstName;
	private String lastName;
	private LocalDate birthday;
	private Sex sex;
	private Clock clock;
	
	public Person(String firstName, String lastName, LocalDate birthday, Sex sex) {
		setFirstName(firstName);
		setLastName(firstName);
		setBirthday(birthday);
		setSex(sex);
		clock  = Clock.systemDefaultZone();
	}
	
	public void setFirstName(String firstName) {
		if (firstName == null) {
			throw new IllegalArgumentException("firstName must not be null!");
		}
		if ("".equals(firstName.trim())) {
			throw new IllegalArgumentException("firstName must not empty or whitespaces only!");
		}
		this.firstName = firstName.trim();
	}

	public void setLastName(String lastName) {
		if (lastName == null) {
			throw new IllegalArgumentException("lastName must not be null!");
		}
		if ("".equals(lastName.trim())) {
			throw new IllegalArgumentException("lastName must not empty or whitespaces only!");
		}
		this.lastName = lastName.trim();
	}
	
	public String getAge()
	{
		LocalDate heute =now();
		
		int alter = heute.get(ChronoField.YEAR) - birthday.get(ChronoField.YEAR);
		String x = ""+alter;
		return x;
	}
	

	public LocalDate now() 
	{
		return LocalDate.now(clock);
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}
	
	public void setSex(Sex sex) {
		this.sex = sex;
	}

	public Sex getSex() {
		return sex;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	

	public LocalDate getBirthday() {
		return birthday;
	}
	public String getBirthdayString() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy.MM.dd").withZone(ZoneId.of("UTC"));
		return dtf.format(birthday);
	}
	
	@Override
	public String toString() {
		return String.format("firstName=%s, lastName=%s, birthday=%s, sex=%s%n", firstName, lastName, getBirthdayString(), sex);
	}

	
	
    
}
