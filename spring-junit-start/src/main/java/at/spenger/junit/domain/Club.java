package at.spenger.junit.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Club {
	private List<Person> l;
	
	public Club() {
		l = new ArrayList<>();
	}
	
	public boolean enter(Person p) {
		if (p == null) {
			throw new IllegalArgumentException("person is null!");
		}
		return l.add(p);
	}
	
	public int numberOf() {
		return l.size();
	}
	
	public List<Person> getPersons() {
		return Collections.unmodifiableList(l);
	}
	
	public int durchschnittsalter()
	{
		LocalDate ld = null;
		int alter = 0;
		for(int x =0; x<l.size(); x++)
		{
			int aktuell = Integer.parseInt(ld.now().toString().substring(0,4));
			int person = Integer.parseInt(l.get(x).getBirthday().toString().substring(0, 4));
			alter =alter +aktuell-person;
		}
		alter = alter/l.size();
		System.out.println("Das Durchschnittsalter betr�gt "+ alter +" Jahre");
		return alter;  
	}
	
	public void sortieren()
	{
		Collections.sort(l, new Sort());
	}

	public void sortStreamAlter()
	{
		l = l.stream()
				.sorted((x,y) -> x.getAge().compareTo(y.getAge()))
				.collect(Collectors.toList());
	}
	
	public void sortStreamName()
	{
		l = l.stream()
				.sorted((x,y) -> x.getLastName().compareTo(y.getLastName()))
				.collect(Collectors.toList());
	}
	
	public int anzahl()
	{
		return l.size();
	}

	public Person aeltester()
	{
		int x =1;
		int aeltester=Integer.parseInt(l.get(0).getBirthday().toString().substring(0, 4));
		int aeltester_array=0;
		for(; x<l.size(); x++)
		{
			int person = Integer.parseInt(l.get(x).getBirthday().toString().substring(0, 4));
			if(person<aeltester)
			{
				aeltester = person;
				aeltester_array=x;
			}
		}
		return l.get(aeltester_array);
	}
	
	public void austreten(int id)
	{
		l.remove(id);
	}
	
	public void sortName() {
		l= l.stream().sorted((x,y) -> x.getLastName().compareTo(y.getLastName())).collect(Collectors.toList());
		
	}
}
