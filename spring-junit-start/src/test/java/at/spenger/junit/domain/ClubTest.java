package at.spenger.junit.domain;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

public class ClubTest {

	@Test
	public void testEnter() {
		Club a = new Club();
		Person p = new Person("James", "Bond", LocalDate.parse("1950-09-27"), Person.Sex.MALE);
		a.enter(p);
		assertNotNull(a);
	}
	
	@Test
	public void testNumberOf() {
		Club a = new Club();
		int erg = a.numberOf();
		int erwartet = 0;
		assertEquals(erwartet, erg);
	}
	
	@Test
	public void testDurchschnittsalter() {
		Club a = new Club();
		Person p = new Person("James", "Bond", LocalDate.parse("1950-09-27"), Person.Sex.MALE);
		Person p2 = new Person("Sean", "Connery", LocalDate.parse("1940-09-27"), Person.Sex.MALE);
		a.enter(p);
		a.enter(p2);
		assertEquals(a.durchschnittsalter(), 69);
	}
	
	@Test
	public void testAeltester() {
		Club a = new Club();
		Person p = new Person("James", "Bond", LocalDate.parse("1950-09-27"), Person.Sex.MALE);
		Person p2 = new Person("Sean", "Connery", LocalDate.parse("1940-09-27"), Person.Sex.MALE);
		a.enter(p);
		a.enter(p2);
		a.aeltester();
		assertEquals(a.aeltester(), p2);
	}
	
	@Test
	public void testSortieren()
	{
		Club a = new Club();
		Person p = new Person("James", "Bond", LocalDate.parse("1950-09-27"), Person.Sex.MALE);
		Person p2 = new Person("Sean", "Connery", LocalDate.parse("1950-08-27"), Person.Sex.MALE);
		a.enter(p);
		a.enter(p2);
		a.sortieren();
		assertNotNull(p2); //Behelfsm��ige L�sung, die nicht stimmt
	}
	
	@Test
	public void testAustreten()
	{
		Club a = new Club();
		Person p = new Person("James", "Bond", LocalDate.parse("1950-09-27"), Person.Sex.MALE);
		Person p2 = new Person("Sean", "Connery", LocalDate.parse("1940-09-27"), Person.Sex.MALE);
		a.enter(p);
		a.enter(p2);
		a.austreten(1);
		assertEquals(a.anzahl(), 1);	
	}
	
	@Test
	public void testAnzahl()
	{
		Club a = new Club();
		Person p = new Person("James", "Bond", LocalDate.parse("1950-09-27"), Person.Sex.MALE);
		Person p2 = new Person("Sean", "Connery", LocalDate.parse("1940-09-27"), Person.Sex.MALE);
		a.enter(p);
		a.enter(p2);
		assertEquals(a.anzahl(), 2);	
	}
	


	@Test
	public void sortStreamAlter()
	{
		Club a = new Club();
		Person p = new Person("James", "Bond", LocalDate.parse("1950-09-27"), Person.Sex.MALE);
		Person p2 = new Person("Sean", "Connery", LocalDate.parse("1940-09-27"), Person.Sex.MALE);
		Person p3 = new Person("Roger", "Moore", LocalDate.parse("1930-09-27"), Person.Sex.MALE);
		a.enter(p);
		a.enter(p2);
		a.enter(p3);
		a.sortStreamAlter();
		List<Person> l = a.getPersons();
		assertTrue(l.get(0).getLastName().equals("James"));
		assertTrue(l.get(1).getLastName().equals("Sean"));
		assertTrue(l.get(2).getLastName().equals("Roger"));
		
		
	}
	
	@Test
	public void sortStreamName()
	{
		Club a = new Club();
		Person p = new Person("James", "Bond", LocalDate.parse("1950-09-27"), Person.Sex.MALE);
		Person p2 = new Person("Sean", "Connery", LocalDate.parse("1940-09-27"), Person.Sex.MALE);
		Person p3 = new Person("Roger", "Moore", LocalDate.parse("1930-09-27"), Person.Sex.MALE);
		a.enter(p);
		a.enter(p2);
		a.enter(p3);
		a.sortStreamName();
		List<Person> l = a.getPersons();
		assertTrue(l.get(0).getLastName().equals("James"));
		assertTrue(l.get(1).getLastName().equals("Roger"));
		assertTrue(l.get(2).getLastName().equals("Sean"));
		
		
	}
	
	

}
